pkgs <- c(
  # 2 installation
  'microbenchmark',
  'lubridate',


  # 4
  'reshape2',
  'data.table',
  'rmarkdown',
  'bookdown',
  'blogdown',
  'plotly',
  'rvest',

  'openxlsx',
  'readxl',
  'haven',
  'XML',
  'bench',
  'foreach',
  'doParallel',
  #
  'lobstr',
  'pryr',

  'futile.logger',
  'lgr',
  'profvis',
  'namer',
  'RSQLite'
)

install.packages('RSQLite')

for (i in pkgs) install.packages(i)


libcurl4-openssl-dev
libssl-dev
libxml2-dev

libgit2-dev

sudo apt install openjdk-14-jre
sudo apt install openjdk-14-jdk
sudo R CMD javareconf JAVA_HOME=/usr/lib/jvm/java-14-openjdk-amd64/
# LD_LIBRARY_PATH=/usr/lib/jvm/java-14-openjdk-amd64/lib/server


sudo apt-get install software-properties-common
