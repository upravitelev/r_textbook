# Начало работы в R {-#installation}

Среда разработки R состоит из нескольких ключевых компонентов: интерпретатор языка, математические библиотеки, компиляторы Fortran/C/C++ и различные пользовательские пакеты.

Соответственно, установка R так или иначе включает в себя установку этих компонентов. Наиболее часто используемая схема --- это установка языка с сайта проекта и одновременно с этим базового компилятора (GNU R) и стандартных библиотек для работы с алгоритмами линейной алгебры (BLAS/LAPACK).

В зависимости от операционной системы, компиляторы Fortran/C/C++ уже могут быть установлены (\*nix-системы), пользователям Windows рекомендуется скачать и установить Rtools. Впрочем, некоторые пакеты при установке по умолчанию также устанавливают Rtools (если речь идет о Windows).

Выполнение этих этапов уже позволит работать в R, однако почти всегда необходимы дополнительные пакеты. Пакеты ставятся уже из среды R средствами языка. Некоторые пакеты требуют наличия дополнительных библиотек или установки окружений для выполнения кода на других языках, но это уже стоит решать при возникновении таких ситуаций, а не при первой установке R.

Базовый R имеет собственный графический интерфейс (правда, только для Windows), тем не менее для более комфортной интерактивной работы обычно устанавливают какую-либо самостоятельную IDE (RStudio, JetBrains plugin, Visual Studio Code и т. д.).

Таким образом, подготовка к работе включает в себя следующие этапы:

-   установка языка R с сайта (<https://www.r-project.org/>, для каждой OS есть соответствующие разделы); опционально --- установка альтернативного интерпретатора и/или приложений, в которые он интегрирован;

-   (опционально) установка альтернативных математических библиотек;

-   установка [*Rtools*](https://cran.r-project.org/bin/windows/Rtools/) (для Windows), пользователи \*nix-систем могут по желанию поменять базовый для своей системы набор компиляторов (например, `gcc` на `clang`);

-   установка IDE (если предполагается интерактивная работа);

-   установка необходимых пакетов и их зависимостей.

## Интерпретаторы {-#installation-interpreters}

Существует несколько интерпретаторов языка R с разной функциональностью и степенью разработанности. Как правило, разработка новых интерпретаторов направлена на увеличение производительности или на расширение функционала.

Некоторые из них давно перестали разрабатываться (`Rho/CXXR`, `Riposte`), другие интерпретаторы, которые разрабатываются частными компаниями, продолжают изредка обновляться (`TERR`, `Renjin`, `pqR`). Некоторые наработки `pqR` в своё время были даже включены в базовый интерпретатор, при этом `pqR` развивается и в настоящее время, хоть и медленными темпами.

<!-- Старые: -->

<!-- - [Rho](https://github.com/rho-devel/rho) -->

<!-- - [Riposte](https://github.com/jtalbot/riposte) -->

<!-- - [pqR](http://www.pqr-project.org/) -->

<!-- - [Renjin](https://github.com/bedatadriven/renjin) -->

Актуальные (на момент написания) интерпретаторы:

-   [*GNU-R*](https://www.r-project.org/) --- базовый открытый интерпретатор, написан на смеси R, C и Fortran. Разрабатывается командой R Foundation и является интерпретатором по умолчанию.

-   [*FastR*](https://github.com/oracle/fastr) --- по уверениям разработчиков, быстрый (минимум в три раза быстрее GNU-R) интерпретатор, реализованный в виртуальной машине GraalVM. Направленность GraalVM на скорость и возможность взаимодействия разных языков с одними и теми же объектами позволяет достаточно просто вызывать и применять R-код к объектам в Python (или других языках экосистемы GraalVM), а также встраивать R-код в приложения на Java.

-   [*TERR*](https://docs.tibco.com/products/tibco-enterprise-runtime-for-r) --- TIBCO Enterprise Runtime for R, основанный на Java интерпретатор от компании TIBCO. Компания занимается разработкой программного обеспечения, в том числе и BI-систем. Так как интерпретатор проприетарный, столкнуться с ним можно только при работе с продуктами TIBCO.

-   [*Microsoft R Open*](https://mran.microsoft.com/open) --- интерпретатор, ранее известный как Revolution R Open, основной продукт компании Revolution Analytics. Revolution R Open создавался как среда многопоточных вычислений для бизнес-компаний, в частности, фармацевтического и биотехнологического секторов. В 2015 году Microsoft поглотила Revolution Analytics, интерпретатор был переименован в Microsoft R Open, а вокруг него стала выстраиваться целая экосистема --- репозиторий MRAN, интеграция с Visual Studio Code, интеграция с библиотекой MKL и так далее. Возможно, через некоторое время Microsoft R Open будет включён в инфраструктуру платформы Microsoft Azure.

Несмотря на привлекательность некоторых решений, у многих интерпретаторов есть одна сложная проблема --- совместимость с пакетами, размещёнными в CRAN. Разработчики, конечно же, стараются тестировать и в ручном, и в автоматическом режиме, как работают их интерпретаторы с CRAN-пакетами. Так или иначе, это может быть долгая работа, несмотря на большой, но всё же ограниченный круг наиболее используемых пакетов.

## BLAS / LAPACK {-#installation-blas}

Немалая часть статистических операций основывается на алгоритмах линейной алгебры. R использует в вычислениях стандарт BLAS API (Basic Linear Algebra Subprograms) и основанную на нём библиотеку LAPACK (Linear Algebra PACKage).

Узнать, какие версии BLAS и LAPACK установлены в системе, можно в `sessionInfo()`:


```r
sessionInfo()$BLAS
```

```
## [1] "/usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0"
```

```r
sessionInfo()$LAPACK
```

```
## [1] "/usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0"
```

Базовые реализации обладают рядом ограничений, в частности, однопоточностью и фиксированностью некоторых параметров (например, размером блоков). Поэтому для низкоуровневой оптимизации вычислений можно воспользоваться альтернативами:

-   ATLAS --- Automatically Tuned Linear Algebra Software, высокопроизводительная реализация BLAS с некоторыми функциями из LAPACK, которая может быть использована на разных платформах.

-   OpenBLAS --- open-source реализация BLAS, форк GotoBLAS, может использоваться с Intel MKL. По некоторым бенчмаркам может быть на порядок быстрее BLAS.

-   cuBLAS --- созданная NVIDIA библиотека для выполнения процедур линейной алгебры на GPU, по некоторым бенчмаркам может быть в 35+ раз быстрее, чем Intel MKL BLAS.

-   Intel MKL --- Math Kernel Library, созданная Intel математическая библиотека, ориентированная на использование в вычислениях процессоров Intel и под разными операционными системами. Включает в себя BLAS, LAPACK, ScaLAPACK и ряд других процедур. Intel MKL используется в Microsoft R Open.

-   AMD ACML --- оптимизированная под AMD-процессоры математическая библиотека (как Intel MKL для Intel-процессоров).

<!-- ## Rtools / gcc+ / clang -->

## Пакеты {-#installation-packages}

Пакеты --- расширения R, содержащие функции, объекты и документацию в стандартизированном формате. Также можно сказать, что это наборы специализированных функций и объектов, предназначенные для решения определённой группы задач.

Например, пакет `ggplot2` используется для создания разнообразных графиков, а пакет `RPostgreSQL` содержит функции создания подключения и работы с PostgreSQL-базой данных.

При установке R устанавливается несколько базовых пакетов, в частности `stats`, `graphics`, `grDevices`, `utils`, `datasets`, `methods`, `base`. Этого вполне достаточно для многих операций: пакет `stats` содержит базовые статистические тесты, пакеты `graphics` и `grDevices` помогают создать и вывести на экран/сохранить основные типы графиков, пакеты `utils`, `methods` и `base` обеспечивают функционирование языка.

Тем не менее, создаются отдельные пакеты для каких-то узких групп статистических методов или, в общем виде, задач, а также для улучшения уже существующих решений. Например, базовый класс таблиц `data.frame` сейчас в чистом виде практически не используется, так как есть более эффективные и удобные альтернативы-надстройки, такие как `data.table`. Также считается хорошей практикой превращать набор рабочих функций в пакеты: как минимум, это обеспечивает простоту их совместного использования с коллегами.

Все пакеты, которые хранятся в официальном репозитории (CRAN), имеют файлы описаний (DESCRIPTION) и документацию в стандартном формате. Многие старые и хорошо проработанные пакеты имеют ещё и пользовательские гайды и виньетки (vignettes, документы с примерами и описаниями основных функций пакета), демопримеры, в некоторых случаях ---подробные онлайн-материалы (это особенно характерно для `tidyverse`-пакетов и Shiny).

### Установка пакетов {-#installation-install}

Для установки пакетов используется функция `install.packages()`, где в аргументах указывается название пакета или вектор названий. В Windows пакеты, как правило, уже представлены в бинарном виде, для \*nix-систем установка пакетов происходит путём сборки из исходников.


```r
# примеры команд установки одного пакета и двух пакетов
install.packages('glue')
install.packages(c('glue', 'httr'))
```

У функции достаточно много других аргументов, наиболее часто используемые --- `repos` для указания репозитория и `dependencies` для указания, надо ли устанавливать все пакеты-зависимости или нет. В некоторых случаях `dependencies = TRUE` может очень замедлить установку, если у пакета достаточно много зависимостей: так, пакет `caret` --- это своего рода метапакет для машинного обучения и полный список используемых им пакетов достигает нескольких десятков. Ещё один аргумент, `lib`, задаёт, в какую папку необходимо устанавливать пакеты. Обычно это либо системная папка (Windows), либо пользовательские папки (Windows, \*nix)

При установке пакетов на Windows надо позаботиться, чтобы либо у процесса были права на запись в системную папку (запускать IDE от имени администратора, например), либо чтобы имя пользователя было латиницей, так как если у процесса не будет прав записи в системную папку, то пользователю будет предложено установить пакет в личную папку пользователя. Если в пути будет кириллица, то высока вероятность, что путь будет не найден и установить пакет не получится. Также можно просто прямо указать, в какую папку следует установить пакет (путь также не должен содержать кириллицу).

Список установленных пакетов можно получить с помощью функции `installed.packages()`, которая возвращает таблицу пакетов и их параметры (версия, требуемая версия R, какие пакеты импортирует при запуске, тип лицензии и т. д.):


```r
installed.packages()[1:5,]
```

```
##           Package     LibPath                                          Version 
## abind     "abind"     "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1" "1.4-5" 
## AER       "AER"       "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1" "1.2-9" 
## apaTables "apaTables" "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1" "2.0.8" 
## arm       "arm"       "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1" "1.12-2"
## askpass   "askpass"   "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1" "1.1"   
##           Priority
## abind     NA      
## AER       NA      
## apaTables NA      
## arm       NA      
## askpass   NA      
##           Depends                                                                                 
## abind     "R (>= 1.5.0)"                                                                          
## AER       "R (>= 3.0.0), car (>= 2.0-19), lmtest, sandwich (>= 2.4-0),\nsurvival (>= 2.37-5), zoo"
## apaTables "R (>= 3.1.2)"                                                                          
## arm       "R (>= 3.1.0), MASS, Matrix (>= 1.0), stats, lme4 (>= 1.0)"                             
## askpass   NA                                                                                      
##           Imports                                                        
## abind     "methods, utils"                                               
## AER       "stats, Formula (>= 0.2-0)"                                    
## apaTables "stats, utils, methods, car, broom, dplyr, boot, tibble, MBESS"
## arm       "abind, coda, graphics, grDevices, methods, nlme, utils"       
## askpass   "sys (>= 2.1)"                                                 
##           LinkingTo
## abind     NA       
## AER       NA       
## apaTables NA       
## arm       NA       
## askpass   NA       
##           Suggests                                                                                                                                                                                                                                                            
## abind     NA                                                                                                                                                                                                                                                                  
## AER       "boot, dynlm, effects, fGarch, forecast, foreign, ineq,\nKernSmooth, lattice, longmemo, MASS, mlogit, nlme, nnet, np,\nplm, pscl, quantreg, rgl, ROCR, rugarch, sampleSelection,\nscatterplot3d, strucchange, systemfit (>= 1.1-20), truncreg,\ntseries, urca, vars"
## apaTables "testthat, knitr"                                                                                                                                                                                                                                                   
## arm       NA                                                                                                                                                                                                                                                                  
## askpass   "testthat"                                                                                                                                                                                                                                                          
##           Enhances License                      License_is_FOSS
## abind     NA       "LGPL (>= 2)"                NA             
## AER       NA       "GPL-2 | GPL-3"              NA             
## apaTables NA       "MIT License + file LICENSE" NA             
## arm       NA       "GPL (> 2)"                  NA             
## askpass   NA       "MIT + file LICENSE"         NA             
##           License_restricts_use OS_type MD5sum NeedsCompilation Built  
## abind     NA                    NA      NA     "no"             "4.1.2"
## AER       NA                    NA      NA     "no"             "4.1.2"
## apaTables NA                    NA      NA     "no"             "4.1.2"
## arm       NA                    NA      NA     "no"             "4.1.2"
## askpass   NA                    NA      NA     "yes"            "4.1.0"
```

Для обновления пакетов есть функция `update.packages()` --- она анализирует, у каких пакетов есть более новые версии, и предлагает пользователю обновить их. Если коллекция пакетов давно не обновлялась, есть смысл выставить аргумент `ask = FALSE`, так как в противном случае будет требоваться подтверждение обновления каждого пакета.

Удалить пакеты можно с помощью функции `remove.packages()`:


```r
remove.packages('glue')
```

<!-- ```{r, include=FALSE} -->

<!-- install.packages('glue') -->

<!-- ``` -->

### Репозитории {-#installation-repositories}

В настоящее время есть четыре крупных репозитория, из которых можно скачать и установить пакеты. Выбор репозитория зависит в основном от задач и от рабочего окружения. Чаще всего используются пакеты, размещенные в CRAN и на github.

-   [*CRAN*](https://cran.r-project.org) --- The Comprehensive R Archive Network. Основной и единственный официальный репозиторий R-пакетов. В CRAN нельзя просто так отправить пакет, все новые пакеты (как и новые версии пакетов) проходят автоматические тесты и просматриваются командой R Foundation. Репозиторий имеет несколько десятков зеркал по всему миру, поэтому можно выбрать ближайшее. Также на сайте репозитория есть списки пакетов (CRAN Task Views), где доступные пакеты сгруппированы по темам (Bayesian, Databases, Graphics, Web и т. д.) и коротко аннотированы.

-   [*github*](http://github.com/) --- авторы многих пакетов предоставляют доступ к dev-версиям своих пакетов в github-репозитории. Нередко бывает так, что в CRAN лежит одна версия, а в github-репозитории --- другая, более новая или ещё не до конца оттестированная. Впрочем, нередки ситуации, когда пакет доступен только на github. Помимо github, пакеты также могут быть размещены на других платформах (gitlab, bitbucket) или хостингах проектов. Для установки пакетов с github обычно используют функцию `install_github()` пакета `remotes`. Аналогично `install_gitlab()` и `install_bitbucket()` для gitlab и bitbucket.

-   [*Bioconductor*](https://www.bioconductor.org/) --- репозиторий сообщества проекта Bioconductor. Так же, как и в CRAN, пакеты тестируются и просматриваются. В основном содержит пакеты, предназначенные для решения задач в области биоинформатики. Для установки пакетов из репозитория Bioconductor можно воспользоваться как функцией `devtools::install_bioc()`, так и функцией `install()` пакета `BiocManager` (с помощью этого пакета устанавливаются и базовые пакеты проекта Bioconductor).

-   [*MRAN*](https://cran.microsoft.com/) --- Microsoft R Application Network, репозиторий Microsoft R Open. По сути является копией CRAN с одной очень важной особенностью --- это не синхронная копия, а ежедневные снапшоты базы CRAN. Также можно установить пакеты тех версий, которые были доступны в CRAN на определенную дату. В определённой мере это полезно, если есть желание восстановить или поддерживать в изолированном виде систему пакетов для выполнения каких-то скриптов, обновление и поддержка которых не планируется. Пакеты устанавливаются так же, как и с CRAN, но необходимо указать явно репозиторий, особенно если планируется использовать снапшот базы. Например, `install.packages('glue', repos = 'https://cran.microsoft.com/snapshot/2020-03-14/')`.

-   [*R-Forge*](https://r-forge.r-project.org/), [*RForge*](http://www.rforge.net/) --- окружения разработки пакетов (включают в себя SVN, баг-трекер и прочие инструменты), которые также можно указать как репозитории для установки пакетов: `install.packages("exams", repos = "http://R-Forge.R-project.org")`.

### Подключение пакетов {-#installation-library}

Для подключения пакетов используется функция `library()`, в которую передаётся название пакета. Название функции, как и в целом названия `library` и `package`, может несколько сбивать с толку --- во многих языках программирования внешние коллекции функций как раз и называются библиотеками. Тем не менее, для простоты можно воспользоваться метафорой, что множество установленных в системе пакетов --- это библиотека пакетов (метафорических "книг") и выражение `library(pkgname)` является выбором определенного пакета ("книги") из этой библиотеки.


```r
detach('package:data.table')
```


```r
# подключаем lubridate
library(lubridate)
```

```
## 
## Attaching package: 'lubridate'
```

```
## The following objects are masked from 'package:base':
## 
##     date, intersect, setdiff, union
```

```r
# проверяем, что пакет подключен
grep('lubridate', search(), value = TRUE)
```

```
## [1] "package:lubridate"
```

При подключении пакета происходит анализ пространства имен функций пакета, и если в подключаемом пакете есть функции, идентичные названиям функций из ранее подключённых пакетов, то ранее подключённые функции скрываются и могут быть вызваны при явном указании пакета. Это поведение по умолчанию, сообщения о конфликтах можно отключить аргументом `warn.conflicts = FALSE`, но тогда придётся самостоятельно проверять наличие возможных конфликтов с помощью `conflicts()`.

Например, из сообщения при подключении пакета `lubridate` видно, что функция `date()` базового пакета скрывается и при вызове `date()` будет вызываться функция пакета `lubridate`:


```r
# смотрим, в каких пакетах есть функция date
findFunction('date')
```

```
## [[1]]
## <environment: package:lubridate>
## attr(,"name")
## [1] "package:lubridate"
## attr(,"path")
## [1] "/home/konhis/R/x86_64-pc-linux-gnu-library/4.1/lubridate"
## 
## [[2]]
## <environment: base>
```

```r
# вызываем скрытую функцию date()
base::date()
```

```
## [1] "Fri Feb  4 18:53:46 2022"
```

В некоторых скриптах, особенно в написанных давно или за авторством не очень опытных пользователей, можно встретить подключение пакетов с помощью `require()`. В настоящее время настоятельно не рекомендуется пользоваться этой функцией. Основная причина в том, что при отсутствии пакета `require()`, интерпретатор выдаст пользователю предупреждение/warning, что такого пакета нет. При использовании `library()` выполнение скрипта будет остановлено с ошибкой. Особенно важно это может быть при отключённых предупреждениях или когда скрипт передаётся другому пользователю (или вообще выполняется неинтерактивно).


```r
require(noname)
```

```
## Loading required package: noname
```

```
## Warning in library(package, lib.loc = lib.loc, character.only = TRUE,
## logical.return = TRUE, : there is no package called 'noname'
```

```r
library(noname)
```

```
## Error in library(noname): there is no package called 'noname'
```

По умолчанию функция `library()` принимает только одно название пакета, в него нельзя передать вектор из названий подключаемых пакетов. В качестве альтернативы можно подключить список пакетов в цикле с проверкой на то, установлен пакет или нет, и прочими информационными сообщениями. Пример такого цикла:


```r
packages_list <- c('data.table', 'ggplot2', 'noname')
for (i in packages_list) {
  if (!(i %in% installed.packages()[, 1])) {
    msg <- paste('Пакет ', i, 'не установлен!')
    stop(msg, call. = FALSE)
  }
  library(i, character.only = TRUE, verbose = FALSE)
  message(paste('Пакет ', i, 'подключен'))
}
```

```
## 
## Attaching package: 'data.table'
```

```
## The following objects are masked from 'package:lubridate':
## 
##     hour, isoweek, mday, minute, month, quarter, second, wday, week,
##     yday, year
```

```
## Пакет  data.table подключен
```

```
## Пакет  ggplot2 подключен
```

```
## Error: Пакет  noname не установлен!
```

Отключить ранее подключённый пакет можно с помощью функции `detach()`, в которой в формате `package:pkgname` надо указать отключаемый пакет:


```r
# отключаем пакет
detach('package:lubridate')

# проверяем
grep('lubridate', search(), value = TRUE)
```

```
## character(0)
```

## Среды разработки и запуск R-скриптов {-#installation-ide-run}

### Работа с R в командной строке {-#installation-cli}

Также R можно запустить в командной строке. Иногда это полезно для короткого тестирования, или же когда предполагается выполнение скриптов на удалённом сервере и запустить графический интерфейс нет никакой возможности. В целом такой запуск схож с работой в консоли любой IDE, но без сопутствующих приятных и комфортных инструментов вроде автодополнения.


```bash
konhis@kergma:~$ R

R version 3.6.3 (2020-02-29) -- "Holding the Windsock"
Copyright (C) 2020 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> print("hello")
[1] "hello"
> q()
Save workspace image? [y/n/c]: n
```

Чаще, конечно, командная строка используется для запуска скриптов по расписанию. Для этого есть интерфейс `Rscript` со своим набором аргументов и опций. В общем виде команда в командной строке с использованием `Rscript` имеет такой вид:


```r
Rscript [options] [-e expr [-e expr2 ...] | file] [args]
```

В опциях может быть указано `--verbose` --- детализация всех процессов, а также `--default-packages=list`, где `list` --- список пакетов через запятую, которые будут загружены перед выполнением скрипта.

Таким образом, с помощью `Rscript` можно и просто выполнять выражения на R (удобно при написании файлов конфигураций: например, для докер-контейнеров), и выполнять R-скрипты.



Допустим, у нас вот такой скрипт --- в нём есть сохранение в файл первых шести строк датасета `mtcars` и вывод на печать сообщения `hello`.


```bash
cat cli_test.R
```

```
## write.csv(head(mtcars), 'mtcars.R', row.names = FALSE)
## print('hello!')
```

Запустим выполнение скрипта в командной строке с помощью `Rscript` и посмотрим содержание созданного `mtcars.R`:


```bash
Rscript --verbose cli_test.R
cat mtcars.R
```

```
## running
##   '/usr/lib/R/bin/R --no-echo --no-restore --file=cli_test.R'
## 
## [1] "hello!"
## "mpg","cyl","disp","hp","drat","wt","qsec","vs","am","gear","carb"
## 21,6,160,110,3.9,2.62,16.46,0,1,4,4
## 21,6,160,110,3.9,2.875,17.02,0,1,4,4
## 22.8,4,108,93,3.85,2.32,18.61,1,1,4,1
## 21.4,6,258,110,3.08,3.215,19.44,1,0,3,1
## 18.7,8,360,175,3.15,3.44,17.02,0,0,3,2
## 18.1,6,225,105,2.76,3.46,20.22,1,0,3,1
```

Удаляем созданный файл:


```bash
rm mtcars.R
```

У Rscript есть альтернатива, которая активно использовалась ранее --- `R CMD BATCH`. Выполнение скриптов с помощью этого интерфейса выглядит следующим образом:


```bash
R CMD BATCH cli_test.R
cat mtcars.R
```

```
## "mpg","cyl","disp","hp","drat","wt","qsec","vs","am","gear","carb"
## 21,6,160,110,3.9,2.62,16.46,0,1,4,4
## 21,6,160,110,3.9,2.875,17.02,0,1,4,4
## 22.8,4,108,93,3.85,2.32,18.61,1,1,4,1
## 21.4,6,258,110,3.08,3.215,19.44,1,0,3,1
## 18.7,8,360,175,3.15,3.44,17.02,0,0,3,2
## 18.1,6,225,105,2.76,3.46,20.22,1,0,3,1
```

При этом в консоли нет вывода, который был при выполнении скрипта через `Rscript`, строчки `hello`, которую мы в скрипте печатаем с помощью `print()`. Весь вывод, который получается с помощью `R CMD BATCH`, записывается в отдельный файл `*.Rout`.


```bash
cat cli_test.Rout
```

```
## 
## R version 4.1.2 (2021-11-01) -- "Bird Hippie"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
## 
## R is free software and comes with ABSOLUTELY NO WARRANTY.
## You are welcome to redistribute it under certain conditions.
## Type 'license()' or 'licence()' for distribution details.
## 
##   Natural language support but running in an English locale
## 
## R is a collaborative project with many contributors.
## Type 'contributors()' for more information and
## 'citation()' on how to cite R or R packages in publications.
## 
## Type 'demo()' for some demos, 'help()' for on-line help, or
## 'help.start()' for an HTML browser interface to help.
## Type 'q()' to quit R.
## 
## [Previously saved workspace restored]
## 
## > write.csv(head(mtcars), 'mtcars.R', row.names = FALSE)
## > print('hello!')
## [1] "hello!"
## > 
## > 
## > proc.time()
##    user  system elapsed 
##   0.133   0.015   0.138
```

`R CMD BATCH` --- устаревший вариант, который в настоящее время можно не использовать для вызова скриптов в консоли. При этом надо иметь в виду, что `BATCH` --- лишь одна из команд интерфейса `R CMD` (вызова R с определёнными командами). В целом интерфейс `R CMD` в настоящее время используется для настройки и администрирования: в частности, для установки, сборки и тестирования пакетов. А для выполнения скриптов и команд достаточно и `Rscript`.





### Работа с R в IDE {-#installation-ide}

Несмотря на некоторое изящество дизайна окна терминала, вести полноценную разработку на R в командной строке может быть неудобно. Большинство практиков так или иначе используют какие-нибудь IDE (Integrated development environment, интегрированная среда разработки). Практически все популярные IDE для работы с R включают в себя текстовый редактор, горячие клавиши и автодополнение кода, средства отладки, инспектор объектов, поддержку визуализаций и т. д. При установке языка также устанавливается и нативный оконный интерфейс, по сути, текстовый редактор, консоль с возможностью вывода графиков.

Самая популярная в настоящий момент IDE для R - RStudio. Помимо разработки IDE, компания поддерживает большое число R-разработчиков. В частности, доминирующее количество пакетов `tidyverse` написано командой Хэдли Викхэма, а сам Викхэм немало занимается стратегическим развитием языка.

-   RStudio Desktop --- в базовой версии бесплатна, платная версия даёт возможность обратиться в техподдержку, а также ряд дополнительных функций (подробнее в [*условиях*](https://rstudio.com/products/rstudio/download/)). Основные функции IDE --- текстовый процессор, консоль, возможность работать в проектах, интеграция с git и командной строкой, широкая поддержка rmarkdown (в том числе шаблонов и чанков на других языках), подключение и просмотр баз данных, инспектор объектов и поддержка визуализаций, инструменты для рефакторинга и отладки, лёгкий файловый менеджер и поддержка синтаксиса других языков.

-   RStudio Server --- в базовой версии бесплатна, набор инструментов и функций идентичен десктопной версии. Возможности платной версии шире и касаются в основном организации доступа пользователей и инструментов администрирования.

-   RStudio Cloud --- фактически облачный аналог Rstudio Server, позволяет работать в командах и держать проекты онлайн. Есть шпаргалки по пакетам и туториалы. В целом хорошо подходит для образовательных целей (учебные проекты студентов, например), но для полноценной работы может быть недостаточно (довольно жесткие лимиты по памяти), также есть ограничения на количество участников проекта.

Прочие IDE менее популярны, но также используются. Наиболее часто упоминаемые альтернативы RStudio:

-   JetBrains plugin. Доступен в любом или почти в любом продукте JetBrains. От продуктов JetBrains унаследованы хорошие инструменты и в целом подход к рефакторингу кода.

-   Visual Studio Code plugin --- расширение для VS Code. Лёгкий кроссплатформенный редактор, подключить R можно с помощью плагинов из пользовательского магазина расширений (и, как следствие, достаточно ограниченный функционал).

-   R Tools for Visual Studio (RTVS) --- расширение для Visual Studio, включает в себя интерпретатор Microsoft R Open. В VS 2019 не используется.

-   RKward --- достаточно старая, но ещё живая и развивающаяся IDE. Представляет собой нечто среднее между RStudio (но без rmarkdown и некоторых других инструментов) и JASP (оконный интерфейс к статистическим тестам).

-   Jupyter notebook --- в jupyter можно установить ядро R (r в jupyte**r** намекает) и работать так же, как с кодом на Python, --- исполняемые ячейки, автодополнение, поддержка markdown, поддержка визуализаций.

-   Google Colab --- аналог jupyter notebook, только онлайн --- им можно делиться с коллегами, например, как Google-документами или -таблицами.

-   Плагины для Vim, Atom, Sublime: что-то среднее между IDE и командной строкой, обычно поддерживается подсветка синтаксиса, многострочное редактирование и подобные функции.

-   Плагины для сред разработки Emacs (ESS) и Eclipse (statET). Были достаточно популярны до появления RStudio.
